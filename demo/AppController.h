//
//  AppDelegate.h
//  demo
//
//  Created by dev Darius on 25/06/2022.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppController : UIResponder <UIApplicationDelegate>
{}
@property(nonatomic, readonly) RootViewController* viewController;
@end

