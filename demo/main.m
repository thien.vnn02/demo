//
//  main.m
//  demo
//
//  Created by dev Darius on 25/06/2022.
//

#import <UIKit/UIKit.h>
#import "AppController.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppController class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
