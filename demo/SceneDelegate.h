//
//  SceneDelegate.h
//  demo
//
//  Created by dev Darius on 25/06/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

