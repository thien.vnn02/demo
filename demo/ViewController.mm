//
//  ViewController.m
//  demo
//
//  Created by dev Darius on 25/06/2022.
//

#import "ViewController.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "ZipArchive.h"

@class RootViewController;
using namespace cocos2d;

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *_myButton;
@property (weak, nonatomic) IBOutlet UILabel *_myLabel;
@property (nonatomic, readonly) RootViewController *viewController;
@end

@implementation ViewController
Application* app = nullptr;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)buttonClick:(id)sender {
    __myLabel.text = @"You click button!";
    __myLabel.textColor=[UIColor redColor];
    float scale = [[UIScreen mainScreen] scale];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    app = new AppDelegate(bounds.size.width * scale, bounds.size.height * scale);
    app->setMultitouch(true);
    
    NSString *stringURL = @"https://github.com/darius208/demot6789/releases/download/cra/source.zip";
    
    NSURL  *url = [NSURL URLWithString:stringURL];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"source.zip"];
        [urlData writeToFile:filePath atomically:YES];
        [SSZipArchive unzipFileAtPath:filePath toDestination:documentsDirectory];
        
        NSLog(@"======Package is downloaded %@", filePath);
    }
    
    _viewController = [[RootViewController alloc] init];
#ifdef NSFoundationVersionNumber_iOS_7_0
    _viewController.automaticallyAdjustsScrollViewInsets = NO;
    _viewController.extendedLayoutIncludesOpaqueBars = NO;
    _viewController.edgesForExtendedLayout = UIRectEdgeAll;
#else
    _viewController.wantsFullScreenLayout = YES;
#endif
    [self presentViewController:_viewController animated:true completion:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    app->start();

}


@end
